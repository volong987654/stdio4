import 'package:color_button/models/user.dart';

class Event{
  int id;
  String name;
  String pathImage;
  String locationName;
  User organiser;
  String price;
  String link;
  bool isLike;
  bool isGoing;
  Event({
  this.id,
  this.name,
  this.pathImage,
  this.locationName,
  this.timeStart,
  this.timeEnd,
  this.organiser,
  this.price,
  this.link,
  this.isLike,
  this.isGoing,
  });

}