


import 'package:rxdart/rxdart.dart';

class StremRxDart {
  var _isButton1 = BehaviorSubject<bool>.seeded(true);
  Stream<bool> get isButton1 => _isButton1.stream;
  var _isButton2 = BehaviorSubject<bool>.seeded(true);
  Stream<bool> get isButton2 => _isButton2.stream;
  var _isButton3 = BehaviorSubject<bool>.seeded(true);
  Stream<bool> get isButton3 => _isButton3.stream;

  void setisButton1() {
    _isButton1.add(!_isButton1.value);
  }

  void setisButton2() {
    _isButton2.add(!_isButton2.value);
  }

  void setisButton3() {
    _isButton3.add(!_isButton3.value);
  }

 
  void depose() {
    _isButton1.close();
    _isButton2.close();
    _isButton3.close();
  }
}
