class User{
  int id;
  String email;
  String avatar;
  bool isFollowed;
  User({
     this.id,
     this.email,
     this.avatar,
     this.isFollowed,
  });
}
