
import 'package:flutter/material.dart';

import 'home_drave.dart';
import 'home_zumba.dart';


class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Theme.of(context).primaryColor,
        ),
        title: Text(
          'What s On',
          style: TextStyle(
            color: Colors.black,
            fontWeight: FontWeight.bold,
          ),
        ),
        actions: <Widget>[
          IconButton(
              icon: const Icon(Icons.notifications),
              tooltip: 'Increase volume by 10',
              onPressed: () {}),
        ],
      ),
      drawer: CustomDrawer(),
      body: ListView.builder(itemCount: 6, itemBuilder: (context, i) => GestureDetector(
             onTap: () => {
                     Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Zumba()
                        )
                     )
             },
        child: Card(
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                      Text('Zumba Session',style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15.0,
                      ),),
                      Text('Long đẹp trai'),
                    ],
                    ),
                    Icon(
                      Icons.beach_access,
                      color: Colors.blue,
                      size: 36.0,
                    ),
                  ],
                ),
              ),
              Divider(),
              Row(
                children: [
                  Padding(
                    padding:  EdgeInsets.all(8.0),
                    child: Icon(
                      Icons.beach_access,
                      color: Colors.blue,
                      size: 36.0,
                    ),
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('This is title of descripion',style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 15.0,
                      ),),
                      Text('This is descripion'),
                    ],
                  )

                ],
              )
            ],
          ),
        ),
      )),
    );
  }
}
