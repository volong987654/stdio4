

import 'package:flutter/material.dart';
import 'home_page.dart';
import 'home_threebutton.dart';

class Zumba extends StatelessWidget {
   bool _hasBeenPressed = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(),
       body:   ListView(
            children: [
              Column(
                children: [
            Container(
              width: double.infinity,
              height: 200.0,
              decoration: BoxDecoration(
                  image: DecorationImage(
                      image: NetworkImage(
                          "https://images.unsplash.com/photo-1579202673506-ca3ce28943ef"),
                      fit: BoxFit.cover)),
              child: Stack(
                children: [
                  Container(
                    margin: EdgeInsets.only(left: 8, top: 8),
                    height: 40,
                    width: 40,
                    child: ClipOval(
                      child: Material(
                        color: Colors.white,
                        child: InkWell(
                          // inkwell color

                          child: InkWell(
                              // inkwell color
                              child: SizedBox(
                                  width: 30,
                                  height: 30,
                                  child: Icon(Icons.close)),
                              onTap: () => {
                                    Navigator.pop(
                                        context,
                                        MaterialPageRoute(
                                            builder: (context) => HomePage()))
                                  }),
                        ),
                      ),
                    ),
                  ),
                  Container(
                    //Chọn vị trí của container ở Dưới (Bottom) và Trái (Left) của Stack
                    alignment: Alignment.topRight,
                    child: Container(
                        margin: EdgeInsets.only(right: 8, top: 8),
                        height: 40,
                        width: 40,
                        child: ClipOval(
                          child: Material(
                            color: Colors.white, // button color
                            child: InkWell(
                              // inkwell color
                              child: SizedBox(
                                  width: 30,
                                  height: 30,
                                  child: Icon(Icons.home)),
                              onTap: () {},
                            ),
                          ),
                        )),
                  ),
                  Positioned(
                      //Chọn vị trí của container cách lề phải 30px, lề dưới 50px
                      left: 5,
                      bottom: 5,
                      child: Container(
                        // padding: EdgeInsets.only(bottom:5.0),

                        width: 60,
                        height: 20,
                        color: Colors.grey[100],
                        child: Text(
                          'Free',
                          style: TextStyle(
                            color: Colors.black,
                          ),
                          textAlign: TextAlign.center,
                        ),
                      )),
                ],
              ),
            ),
            Padding(
              padding: EdgeInsets.only(left: 15.0, top: 20),
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('12 Now 2020,    7:30pm',
                        style: TextStyle(
                          color: Colors.red,
                          fontSize: 12,
                        )),
                    SizedBox(
                      height: 10,
                    ),
                    Text('Zumba secion',
                        style: TextStyle(
                            color: Colors.black,
                            fontSize: 16,
                            fontWeight: FontWeight.bold)),
                    SizedBox(
                      height: 10,
                    ),
                    Text('Zumba secion Zumba secion Zumba secion ',
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 14,
                        )),
                    SizedBox(
                      height: 10,
                    ),
                     HomeThreeButton(),
                 
                   // bu
                    SizedBox(
                      height: 10,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                            'Được dịch từ tiếng Anh-Trong xuất bản và thiết kế'),
                        Text(
                            'Được dịch từ tiếng Anh-Trong xuất bản và thiết kế Lorem ipsum '),
                        Text('Được dịch từ tiếng Anh-Trong xuất bản '),
                        Text('Được dịch từ tiếng Anh-Trong xuất bản và '),
                        Padding(
                          padding: EdgeInsets.only(top: 8.0, bottom: 8.0),
                          child: Text(
                            'Show more',
                            style: TextStyle(color: Colors.blue),
                          ),
                        ),
                      ],
                    )
                  ]),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Container(
                        margin: EdgeInsets.only(left: 15),
                        width: 30.0,
                        height: 30.0,
                        decoration: new BoxDecoration(
                            shape: BoxShape.circle,
                            image: new DecorationImage(
                                fit: BoxFit.fill,
                                image: NetworkImage(
                                    "https://images.unsplash.com/photo-1579202673506-ca3ce28943ef")))),
                    Text('King Of Trivia')
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(right: 20),
                  child: RaisedButton.icon(
                    onPressed: () {},
                    icon: Icon(Icons.mail),
                    label: Text('Flower'),
                    color: Colors.grey[300],
                  ),
                )
              ],
            ),
            Divider(),
            Container(
              margin: EdgeInsets.only(left: 15),
              child: Align(
                alignment: Alignment
                    .topLeft, // Align however you like (i.e .centerRight, centerLeft)
                child: Text("Friend extending"),
              ),
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Row(
                  children: [
                    Container(
                        margin: EdgeInsets.only(left: 15),
                        width: 30.0,
                        height: 30.0,
                        decoration: new BoxDecoration(
                            shape: BoxShape.circle,
                            image: new DecorationImage(
                                fit: BoxFit.fill,
                                image: NetworkImage(
                                    "https://images.unsplash.com/photo-1579202673506-ca3ce28943ef")))),
                  ],
                ),
                Container(
                  margin: EdgeInsets.only(right: 20),
                  child: RaisedButton.icon(
                    onPressed: () {},
                    icon: Icon(Icons.mail),
                    label: Text('interved friends'),
                    color: Colors.grey[300],
                  ),
                )
              ],
            ),
           SizedBox(height: 200.0,),
              Container(
                height: 50.0,
                decoration: BoxDecoration(      
                color: Colors.blue, ),
                 child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,          
                    children: [                                          
                      Text("RSVP",style: TextStyle(fontWeight: FontWeight.w700,fontSize: 20.0,color: Colors.white),),
                    ],
                ),
              ),

          ],
           
              )
            ],
          ),
        
        
    );
  }

}
