
import 'package:color_button/models/stream.dart';
import 'package:flutter/material.dart';

class HomeThreeButton extends StatefulWidget {
  @override
  _HomeThreeButton createState() => _HomeThreeButton();
}

class _HomeThreeButton extends State<HomeThreeButton> {
 StremRxDart _stremRxDart = StremRxDart();

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceAround,
      children: [
        StreamBuilder<bool>(
            stream: _stremRxDart.isButton1,
            initialData: false,
            builder: (context, snapshot) {
              print(snapshot.data);
              return OutlinedButton(
                style: ButtonStyle(
                backgroundColor: snapshot.data == true
                      ? MaterialStateProperty.all(Colors.grey[300])
                      : MaterialStateProperty.all(Colors.blue),
                  shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5))),
                ),
                child: Row(
                  children: [
                    Text(
                      "I'm going!",
                      style: TextStyle(fontSize: 20, color: Colors.black),
                    )
                  ],
                ),
                onPressed: () {
                  _stremRxDart.setisButton1();
                },
              );
            }),
        StreamBuilder<bool>(
            stream: _stremRxDart.isButton2,
            initialData: false,
            builder: (context, snapshot) {
              print(snapshot.data);
              return OutlinedButton(
                style: ButtonStyle(
                backgroundColor: snapshot.data == true
                      ? MaterialStateProperty.all(Colors.grey[300])
                      : MaterialStateProperty.all(Colors.blue),
                  shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5))),
                ),
                child: Row(
                  children: [
                    Text(
                      "Tim",
                      style: TextStyle(fontSize: 20, color: Colors.black),
                    )
                  ],
                ),
                onPressed: () {
                  _stremRxDart.setisButton2();
                },
              );
            }),
        StreamBuilder<bool>(
            stream: _stremRxDart.isButton3,
            initialData: false,
            builder: (context, snapshot) {
              print(snapshot.data);
              return OutlinedButton(
                style: ButtonStyle(
                backgroundColor: snapshot.data == true
                      ? MaterialStateProperty.all(Colors.grey[300])
                      : MaterialStateProperty.all(Colors.blue),
                  shape: MaterialStateProperty.all(RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(5))),
                ),
                child: Row(
                  children: [
                    Text(
                      "rẽ phải",
                      style: TextStyle(fontSize: 20, color: Colors.black),
                    )
                  ],
                ),
                onPressed: () {
                  _stremRxDart.setisButton3();
                },
              );
            }),
        
      ],
    );
  }
}
